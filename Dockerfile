FROM alpine:latest

ENTRYPOINT ["/bin/sh", "-c"]
CMD sleep 1d

RUN apk update && \
    apk add --no-cache \
      zsh \
      git \
      curl \
      py3-pip

RUN python3 -m pip install --upgrade ranger-fm

RUN printf 's@^(root:.+):.+$@\\1:%s@g' "$(command -v zsh)" | \
    xargs -0 -I {} sed -ri {} /etc/passwd

RUN git clone --depth=1 https://github.com/romkatv/powerlevel10k.git ~/powerlevel10k

COPY .zshrc /root/
COPY .p10k.zsh /root/
COPY plugins /usr/share/zsh/plugins
