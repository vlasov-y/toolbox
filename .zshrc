source ~/powerlevel10k/powerlevel10k.zsh-theme

# To customize prompt, run `p10k configure` or edit ~/.p10k.zsh.
[[ ! -f ~/.p10k.zsh ]] || source ~/.p10k.zsh

## p10k customization
# disable extension "ranger" on the first line in the right
typeset -g _p9k__1rranger=

## ZSH options
setopt correct                                                  # Auto correct mistakes
setopt extendedglob                                             # Extended globbing. Allows using regular expressions with *
setopt nocaseglob                                               # Case insensitive globbing
setopt rcexpandparam                                            # Array expension with parameters
setopt nocheckjobs                                              # Don't warn about running processes when exiting
setopt numericglobsort                                          # Sort filenames numerically when it makes sense
setopt nobeep                                                   # No beep
setopt appendhistory                                            # Immediately append history instead of overwriting
setopt histignorealldups                                        # If a new command is a duplicate, remove the older one
setopt autocd                                                   # if only directory path is entered, cd there.
zstyle ':completion:*' matcher-list 'm:{a-zA-Z}={A-Za-z}'       # Case insensitive tab completion
zstyle ':completion:*' list-colors "${(s.:.)LS_COLORS}"         # Colored completion (different colors for dirs/files/etc)
zstyle ':completion:*' rehash true                              # automatically find new executables in path 
# Speed up completions
zstyle ':completion:*' accept-exact '*(N)'
zstyle ':completion:*' use-cache on
zstyle ':completion:*' cache-path ~/.zsh/cache
zstyle ':completion:*' menu select

## History and editor
HISTFILE=~/.zhistory
HISTSIZE=10000
SAVEHIST=1000
WORDCHARS=${WORDCHARS//\/[&.;]}

## Keybindings section
bindkey -e
bindkey '^[[7~' beginning-of-line                               # Home key
bindkey '^[[H' beginning-of-line                                # Home key
if [[ "${terminfo[khome]}" != "" ]]; then
  bindkey "${terminfo[khome]}" beginning-of-line                # [Home] - Go to beginning of line
fi
bindkey '^[[8~' end-of-line                                     # End key
bindkey '^[[F' end-of-line                                     # End key
if [[ "${terminfo[kend]}" != "" ]]; then
  bindkey "${terminfo[kend]}" end-of-line                       # [End] - Go to end of line
fi
bindkey '^[[2~' overwrite-mode                                  # Insert key
bindkey '^[[3~' delete-char                                     # Delete key
bindkey '^[[C'  forward-char                                    # Right key
bindkey '^[[D'  backward-char                                   # Left key
bindkey '^[[5~' history-beginning-search-backward               # Page up key
bindkey '^[[6~' history-beginning-search-forward                # Page down key

# Navigate words with ctrl+arrow keys
bindkey '^[Oc' forward-word                                     #
bindkey '^[Od' backward-word                                    #
bindkey '^[[1;5D' backward-word                                 #
bindkey '^[[1;5C' forward-word                                  #
bindkey '^H' backward-kill-word                                 # delete previous word with ctrl+backspace
bindkey '^[[Z' undo                                             # Shift+tab undo last action
# Use syntax highlighting
source /usr/share/zsh/plugins/zsh-syntax-highlighting/zsh-syntax-highlighting.zsh
# Use history substring search
source /usr/share/zsh/plugins/zsh-history-substring-search/zsh-history-substring-search.zsh
# bind UP and DOWN arrow keys to history substring search
zmodload zsh/terminfo
bindkey "$terminfo[kcuu1]" history-substring-search-up
bindkey "$terminfo[kcud1]" history-substring-search-down
bindkey '^[[A' history-substring-search-up			
bindkey '^[[B' history-substring-search-down

## Autoload
autoload -U compinit colors zcalc
compinit -d
colors
setopt prompt_subst

# Color man pages
export LESS_TERMCAP_mb=$'\E[01;32m'
export LESS_TERMCAP_md=$'\E[01;32m'
export LESS_TERMCAP_me=$'\E[0m'
export LESS_TERMCAP_se=$'\E[0m'
export LESS_TERMCAP_so=$'\E[01;47;34m'
export LESS_TERMCAP_ue=$'\E[0m'
export LESS_TERMCAP_us=$'\E[01;36m'
export LESS=-r

## Variables
export EDITOR=/usr/bin/vim
export VISUAL=/usr/bin/vim

## Completions for 3-d party binaries
for COMMAND in kubectl helm k3d flux; do
  if command -v "$COMMAND" >/dev/null; then
    source <("$COMMAND" completion zsh)
  fi
done

## Aliases
alias ..="cd .. && ls -lhA --color=auto"
alias d='sudo docker'
alias dc="docker-compose" 
alias e="exit"
alias j="sudo journalctl"
alias l="ls -lh --color=auto"
alias la="ls -lhA --color=auto"
alias p="ping 8.8.8.8"
alias please='sudo'
alias reborn="sudo shutdown -r now"
alias s='sudo'
alias sctl="sudo systemctl"
alias svi="sudo vim"
alias svim="sudo vim"
alias t='terraform'
alias k="kubectl"
alias kd="kubectl describe"
alias kg="kubectl get"
alias ky="kubectl get -o yaml"
alias ke='kubectl exec -it'
alias vi="vim"
alias g="git"
alias b="sudo btrfs"
alias c="code ."
alias ansible-time="time ansible-playbook"

## Functions
rustofat() {	
  figlet -f rustofat "$*" | sed "s@^@${P:-#  }@g" | tee /tmp/tt | xsel -i -b
  cat /tmp/tt; rm -f /tmp/tt
}
rusto() {
  figlet -f rusto "$*" | sed "s@^@${P:-#  }@g; s@┆@┘@g" | tee /tmp/tt | xsel -i -b
  cat /tmp/tt; rm -f /tmp/tt
}

## Custom env
if [ -f env.sh ]; then
  source ./env.sh
fi

## Ranger kubeconfig permissions fix
if [ -s "/etc/rancher/rke2/rke2.yaml" ] && [ -z "$KUBECONFIG" ]; then
  export KUBECONFIG=/etc/rancher/rke2/rke2.yaml
	sudo chown "${USER}:${USER}" "$KUBECONFIG"
elif [ -s "/etc/rancher/k3s/k3s.yaml" ] && [ -z "$KUBECONFIG" ]; then
  export KUBECONFIG=/etc/rancher/k3s/k3s.yaml
	sudo chown "${USER}:${USER}" "$KUBECONFIG"
fi

## k9s update check
if command -v k9s 1>/dev/null 2>&1; then
  LOCATION="$(curl -sS https://github.com/derailed/k9s/releases/latest -D- | awk '/^location:/{print $2}')"
  LATEST_VERSION="$(echo "$LOCATION" | awk -F/ '{print $NF}' | grep -oE '[0-9a-zA-Z_.-]+')"
  if ! k9s version --short | grep -qF "$LATEST_VERSION"; then
    printf "Updating k9s to %s\n" "$LATEST_VERSION"
    URL="$(printf 'https://github.com/derailed/k9s/releases/download/%s/k9s_Linux_x86_64.tar.gz' "$LATEST_VERSION")"
    echo "$URL"
    curl -sSL "$URL" | \
    tar -xpzvf - k9s
    install k9s ~/.local/bin/k9s -m 0755
    rm -f k9s
  fi
fi

